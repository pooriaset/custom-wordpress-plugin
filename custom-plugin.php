<?php
/**
 * @package CustomPlugin
 */
/*
Plugin Name: Custom Plugin
Plugin URI: http://customPlugin.org
Description: This is my first custom plugin
version: 1.0.0
Author: Pooria Setayesh
Author URI: http://gitlab.com/pooriaset
License: GPLv2 or later
Text Domain: custom-plugin
*/

defined("ABSPATH") or die("You can't access to this file!");

if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/vendor/autoload.php';
}

define("PLUGIN_PATH", plugin_dir_path(__FILE__));
define("PLUGIN_URL" , plugin_dir_url(__FILE__));
define("PLUGIN" , plugin_basename(__FILE__));

use Inc\Base\Activate;
use Inc\Base\Deactivate;

function activate_custom_plugin(){
    Activate::activate();
}
function deactivate_custom_plugin(){
    Deactivate::deactivate();
}

register_activation_hook(__FILE__ , "activate_custom_plugin");
register_deactivation_hook(__FILE__,"deactivate_custom_plugin");

if (class_exists("Inc\\Init")) {
    Inc\init::register_services();
}
