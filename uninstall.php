<?php

/**
 * Trigger this file when plugin uninstall
 * @package CustomPlugin
 */

if(! defined("WP_UNINSTALL_PLUGIN")){
    die;
}