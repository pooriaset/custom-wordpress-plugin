<?php

/**
 * @package CustomPlugin
 */

namespace Inc;
final class init
{
    /**
     * Store all the classes in an array
     * @return array Full list of classes
     */
    public static function get_all_services()
    {
        return [
            Pages\Admin::class,
            Base\Enqueue::class,
            Base\SettingsLinks::class
        ];
    }

    /**
     * Loop through the classes, initialize them,
     * and class the register() method if it exists
     */
    public static function register_services()
    {
        foreach (self::get_all_services() as $class) {
            $service = self::instantiate($class);
            if (method_exists($service, 'register')) {
                $service->register();
            }
        }
    }

    /**
     * Initialize the class
     */
    private static function instantiate($class)
    {
        return new $class();
    }

}

//use Inc\Base\Activate;
//use Inc\Base\Deactivate;
//use Inc\Pages\Admin;
//
//if (!class_exists("CustomPlugin")) {
//    class CustomPlugin
//    {
//        public $plugin;
//
//        function __construct()
//        {
//            $this->plugin = plugin_basename(__FILE__);
//            add_action('init', array($this, 'custom_post_type'));
//        }
//

//        function custom_post_type()
//        {
//            register_post_type("book", ['public' => true, 'label' => 'Books']);
//        }
//

//
//        function activate()
//        {
//            Activate::activate();
//        }
//
//        function deactivate()
//        {
//            Deactivate::deactivate();
//        }
//    }
//
//
//    if (class_exists("CustomPlugin")) {
//        $customPlugin = new CustomPlugin();
//        $customPlugin->register();
//        $GLOBALS['customPlugin'] = $customPlugin;
//    }
//
//    //activation
//    register_activation_hook(__FILE__, array($customPlugin, "activate"));
//
//    //deactivation
//    register_deactivation_hook(__FILE__, array($customPlugin, "deactivate"));

?>