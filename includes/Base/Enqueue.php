<?php
/**
 * @package CustomPlugin
 */

namespace Inc\Base;
class Enqueue
{
    public function register()
    {
        add_action("admin_enqueue_scripts", array($this, 'enqueue'));
    }

    function enqueue()
    {
        wp_enqueue_style("mainStyles", PLUGIN_URL . "/assets/css/styles.css");
    }
}
