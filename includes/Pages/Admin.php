<?php
/**
 * @package CustomPlugin
 */

namespace Inc\Pages;

class Admin
{
    public function admin_index()
    {
        require_once PLUGIN_PATH . '/templates/admin/index.php';
    }

    public function add_admin_menu_pages()
    {
        add_menu_page("Custom Plugin", "Custom Plugin", "manage_options",
            "custom_plugin", array($this, "admin_index"),"dashicons-store");
    }

    function register(){
        add_action("admin_menu",array($this,"add_admin_menu_pages"));
    }
}
